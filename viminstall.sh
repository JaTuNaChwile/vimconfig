#!/bin/bash

# Colors
ESC_SEQ="\x1b["
COL_RESET=$ESC_SEQ"39;49;00m"
COL_RED=$ESC_SEQ"31;01m"
COL_GREEN=$ESC_SEQ"32;01m"
COL_YELLOW=$ESC_SEQ"33;01m"
COL_BLUE=$ESC_SEQ"34;01m"
COL_MAGENTA=$ESC_SEQ"35;01m"
COL_CYAN=$ESC_SEQ"36;01m"

#=====================================#
#				      # 
# Skrypt instalacyjny Vim Plugins     #
#			              #
#=====================================#




printf $"$COL_BLUE

#=====================================#
#				      # 
# Skrypt instalacyjny Vim Plugins     #
#                                     #
# Wykonał: Patryk Gawroński	      #
# e-mail: theharbinger@opencores.org  #
#                                     #
#=====================================#
$COL_RESET"


mkdir -p ~/.vim/ ~/.vim/autoload ~/.vim/bundle 
mkdir -p ~/.vim/bundle/vim-airline ~/.vim/bundle/syntastic

printf $"$COL_YELLOW Intall $COL_BLUE pathogen$COL_YELLOW 1 of 3 $COL_RESET\n"

mkdir -p ~/.vim/autoload ~/.vim/bundle
cd ~/.vim/autoload
wget -q https://raw.githubusercontent.com/tpope/vim-pathogen/master/autoload/pathogen.vim

printf $"$COL_YELLOW Intall $COL_BLUE vim-airline$COL_YELLOW 2 of 3 $COL_RESET\n"

git clone https://github.com/bling/vim-airline ~/.vim/bundle/vim-airline

cd ~/.vim/bundle
git clone https://github.com/paranoida/vim-airlineish.git



printf $"$COL_YELLOW Intall $COL_BLUE vim-syntastic$COL_YELLOW 3 of 3 $COL_RESET\n"

git clone https://github.com/scrooloose/syntastic.git ~/.vim/bundle/syntastic



printf $"$COL_BLUE Czy podmienić twój vimrc? [T/N]$COL_RESET\n>"

read -n 1 decision

case $decision in
	'T' ) 
		cd ~/
		wget -q https://bitbucket.org/TheHarbinger/vimconfig/raw/b1dfe165e7081909ad363615b28defbcade8b6c1/.vimrc;;
	't' )
		cd ~/
		wget -q https://bitbucket.org/TheHarbinger/vimconfig/raw/b1dfe165e7081909ad363615b28defbcade8b6c1/.vimrc;;

esac
